AWS.config.update({
  accessKeyId: 'AKIAJUOSTASFYWQIT5JQ',
  secretAccessKey: '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',
  region: 'us-west-2'
});

var dynamodb = new AWS.DynamoDB();
var table = new AWS.DynamoDB({
      params: {
          TableName: 'object_detection_logs'
      }
});

const video = document.getElementById("video");

var epoch = 1300000000000;
var generateRowId = function(shardId /* range 0-64 for shard/slot */) {
  var ts = new Date().getTime() - epoch;
  var randid = Math.floor(Math.random() * 512);
  ts = (ts * 64);   // bit-shift << 6
  ts = ts + shardId;
  return (ts * 512) + (randid % 512);
}

var renderPredictions = function(predictions){
  const c = document.getElementById("canvas");
  const ctx = c.getContext("2d");
  ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
  // Font options.
  const font = "16px sans-serif";
  ctx.font = font;
  ctx.textBaseline = "top";
  predictions.forEach(prediction => {
    if(prediction.score > 0.7){
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      const width = prediction.bbox[2];
      const height = prediction.bbox[3];
      // Draw the bounding box.
      ctx.strokeStyle = "#00FFFF";
      ctx.lineWidth = 4;
      ctx.strokeRect(x, y, width, height);
      // Draw the label background.
      ctx.fillStyle = "#00FFFF";
      const textWidth = ctx.measureText(prediction.class).width;
      const textHeight = parseInt(font, 10); // base 10
      ctx.fillRect(x, y, textWidth + 4, textHeight + 4);
    }
  });

  predictions.forEach(prediction => {
    if(prediction.score > 0.7){
      const x = prediction.bbox[0];
      const y = prediction.bbox[1];
      // Draw the text last to ensure it's on top.
      ctx.fillStyle = "#000000";
      ctx.fillText(prediction.class, x, y);

      console.log(prediction)

      prediction.id = "obj_name:" + generateRowId(4);
      //table.putItem({ Item : AWS.DynamoDB.Converter.marshall(prediction) },function(e){
       // console.log(e);
      //});
    }
  });
};

var detectFrame = function(video, model){
  model.detect(video).then(predictions => {
    renderPredictions(predictions);
    requestAnimationFrame(() => {
      if(detectionStarted)
        detectFrame(video, model);
    });
  });
};

var localStream;
var detectionStarted;


var startDetection = function(){
  const webCamPromise = navigator.mediaDevices
    .getUserMedia({
      audio: false,
      video: {
        facingMode: "user",
        width: 600,
        height: 500
      }
    })
    .then(stream => {
      video.srcObject = localStream = stream;
      return new Promise((resolve, reject) => {
        video.onloadedmetadata = () => {
          video.play();
          resolve();
        };
      });
    });

  const modelPromise = cocoSsd.load();
  Promise.all([modelPromise, webCamPromise]).then(values => {
    detectionStarted = true;
    detectFrame(video, values[0]);
  });
}

$('#start-camera').on('click', function () {
  if(!localStream){
    startDetection();
    $(this).text("Stop camera");
  }else{
    localStream.getVideoTracks()[0].stop();
    localStream=null;
    $(this).text("Start camera");
    if(detectionStarted)
      detectionStarted = null;
  }
})

function draw_map(){
  var mapProp= {
    center:new google.maps.LatLng(51.508742,-0.120850),
    zoom:5,
  };
  var map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

  var $this = $("#googleMap")
  $this.width($this.parent().width())
  $this.height($this.parent().height())
}