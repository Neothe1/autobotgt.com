const express = require("express")
const app = express();

var session = require('express-session')
var bodyParser = require('body-parser')
var path = require('path')
var FileStore = require('session-file-store')(session);



app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

app.use(session({ store : new FileStore(), resave: true, saveUninitialized: true, secret: 'uwotm8' }))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))
app.use(express.static(path.join(__dirname, 'public')))

var passport = require("./auth.js")
app.use(passport.initialize())
app.use(passport.session())


//home page
app.get("/",(req,res) => {
	res.render("home",{home : true})
})

app.get("/about",(req,res) => {
	res.render("about")
})

var userRoutes = require("./routes/user.js")
app.use(userRoutes)

var appRoutes = require("./routes/app.js")
app.use(appRoutes)

app.listen(3000, ()=>{
	console.log("node js app is running on port 3000")
})