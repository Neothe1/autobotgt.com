const express = require("express")
const router  = express.Router();

const passport = require("../auth.js")

//login page
router.get('/login', passport.authenticate('auth0', {
  scope: 'openid email profile'
}), function (req, res) {
  res.redirect('/')
});

//authentication
router.get('/login/callback', function (req, res, next) {
  passport.authenticate('auth0', function (err, user, info) {
    if (err) { return next(err); }
    if (!user) { return res.redirect('/') }
    req.logIn(user, function (err) {
      if (err) { return next(err) }
      const returnTo = req.session.returnTo;
      delete req.session.returnTo
      res.redirect(returnTo || '/app')
    });
  })(req, res, next)
});

//logout
router.get('/logout', (req, res) => {
  req.logout()
  res.redirect('/')
});

module.exports = router;