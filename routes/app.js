const express = require("express")
const router  = express.Router();

var authentic = require("../middleware/authentic.js")

router.get("/app",authentic(),(req,res) => {
	res.render("app/index",{name : req.user.displayName});
});

module.exports = router;