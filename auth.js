var passport = require('passport');
var Auth0Strategy = require('passport-auth0');

console.log(process.argv[2]=="prod"?'https://autobotgt.com/login/callback':'http://localhost:3000/login/callback')
var strategy = new Auth0Strategy({
    domain: "autobotgt.auth0.com",
    clientID: "qQSvZ97HlZwbRJiDux8q7SARxu8vszzP",
    clientSecret: "04ghfRIzJ-GNeuCwtYnh6TvlaV5OmEnDpyM6gaEd03VndpnS4NigPRmAguvrjqK6",
    callbackURL: process.argv[2]=="prod"?'https://autobotgt.com/login/callback':'http://localhost:3000/login/callback'
  },
  function (accessToken, refreshToken, extraParams, profile, done) {
    // accessToken is the token to call Auth0 API (not needed in the most cases)
    // extraParams.id_token has the JSON Web Token
    // profile has all the information from the user
    return done(null, profile);
  }
);

passport.serializeUser(function (user, done) {
  done(null, user);
});

passport.deserializeUser(function (user, done) {
  done(null, user);
});

passport.use(strategy);

module.exports = passport